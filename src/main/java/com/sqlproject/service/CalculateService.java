package com.sqlproject.service;

import com.sqlproject.model.Reports;
import com.sqlproject.model.UpdateReports;

import java.io.IOException;
import java.util.List;

public interface CalculateService {
    List<UpdateReports> getAllReports(Integer limit);
    byte[] getReportsZip(List<UpdateReports> reportsList) throws IOException;
}
