package com.sqlproject.service.impl;

import com.sqlproject.model.Reports;
import com.sqlproject.model.UpdateReports;
import com.sqlproject.service.CalculateService;
import com.sqlproject.util.ReportsRowMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
@RequiredArgsConstructor
public class CalculateServiceImpl implements CalculateService {
    private final JdbcTemplate jdbcTemplate;

    private static final String FIND_ALL_REPORTS_WITH_SCORE = "" +
            "WITH nbki_data_distinct as (SELECT client_id,\n" +
            "                                   MAX(update_date) as update_date_time\n" +
            "                            FROM nbki_data\n" +
            "                            GROUP BY client_id),\n" +
            "\n" +
            "     score_distinct as (SELECT client_id,\n" +
            "                               MAX(update_time) as update_date_time\n" +
            "                        FROM scorista_data\n" +
            "                        WHERE score > 0\n" +
            "                        GROUP BY client_id)\n" +
            "\n" +
            "SELECT p.client_id       AS client_id,\n" +
            "       p.last_name       AS last_name,\n" +
            "       p.patronymic      AS patronymic,\n" +
            "       p.first_name      AS first_name,\n" +
            "       nb.credit_history AS credit_history,\n" +
            "       scd.score         AS score\n" +
            "\n" +
            "FROM score_distinct AS sc\n" +
            "         INNER JOIN nbki_data_distinct AS nbk\n" +
            "         INNER JOIN nbki_data AS nb\n" +
            "                    ON nbk.client_id = nb.client_id AND nbk.update_date_time = nb.update_date\n" +
            "                    ON nbk.client_id = sc.client_id\n" +
            "         INNER JOIN scorista_data AS scd\n" +
            "                    ON scd.client_id = sc.client_id AND scd.update_time = sc.update_date_time\n" +
            "         INNER JOIN\n" +
            "     passports AS p ON sc.client_id = p.client_id\n" +
            "GROUP BY p.client_id, p.last_name, p.patronymic, p.first_name, nb.credit_history, scd.score\n" +
            "ORDER BY client_id\n" +
            "LIMIT ?";


    public List<UpdateReports> getAllReports(Integer limit) {
        List<Reports> list = jdbcTemplate.query(FIND_ALL_REPORTS_WITH_SCORE, new Object[]{limit}, new ReportsRowMapper());
        List<UpdateReports> collect = new ArrayList<>();
        list.forEach(x ->
                collect.add(new UpdateReports(
                        checkName(collect,
                                x.getLastName() +
                                        "_" +
                                        x.getFirstName() +
                                        "_" +
                                        x.getPatronymic() +
                                        "_" +
                                        x.getScore()
                        ),
                        x.getCreditHistory()
                )));


        return collect;
    }

    public byte[] getReportsZip(List<UpdateReports> reportsList) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zipOut = new ZipOutputStream(baos);
        reportsList.forEach(x -> {
            try {
                addFileToZip(
                        zipOut,
                        x.getName(),
                        getFile(x.getLoanHistory()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        zipOut.close();
        return baos.toByteArray();
    }

    private byte[] getFile(String fileEncode) {
        return Base64.getDecoder().decode(fileEncode);
    }

    private void addFileToZip(ZipOutputStream zipOut, String fileName, byte[] fileBytes) throws IOException {
        String name = fileName + ".xml";
        ZipEntry file1Entry = new ZipEntry(name);
        zipOut.putNextEntry(file1Entry);
        ByteArrayInputStream file1In = new ByteArrayInputStream(fileBytes);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = file1In.read(buffer)) > 0) {
            zipOut.write(buffer, 0, length);
        }
        zipOut.closeEntry();
    }

    public String checkName(List<UpdateReports> updateReports, String name) {
        String finalName = name;
        boolean nameExists = updateReports.stream()
                .anyMatch(o -> o.getName().equals(finalName));
        if (nameExists) {
            int suffix = 1;
            String baseName = name;
            while (true) {
                String newName = String.format("%d %s", suffix, baseName);
                boolean newNameExists = updateReports.stream()
                        .anyMatch(o -> o.getName().equals(newName));
                if (!newNameExists) {
                    name = newName;
                    break;
                }
                suffix++;
            }

        }
        return name;
    }

}
