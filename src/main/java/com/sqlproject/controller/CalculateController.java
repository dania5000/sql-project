package com.sqlproject.controller;

import com.sqlproject.model.Reports;
import com.sqlproject.model.UpdateReports;
import com.sqlproject.service.CalculateService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

import static org.apache.tomcat.util.http.fileupload.FileUploadBase.CONTENT_DISPOSITION;

@RestController
@RequiredArgsConstructor
public class CalculateController {
    private final CalculateService calculateService;

    @GetMapping("/zip")
    public ResponseEntity<ByteArrayResource> getCreditCalculate() throws IOException {
        List<UpdateReports> allReports = calculateService.getAllReports(1000);
        return ResponseEntity.ok()
                .header(CONTENT_DISPOSITION, "attachment;filename=" + "score_reports" + ".zip")
                .contentType(MediaType.valueOf("application/zip"))
                .body(new ByteArrayResource(calculateService.getReportsZip(allReports)));
    }
}
