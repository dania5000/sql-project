package com.sqlproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Reports {
    private Integer clientId;
    private String lastName;
    private String patronymic;
    private String firstName;
    private String creditHistory;
    private Integer score;

}
