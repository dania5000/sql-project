package com.sqlproject.util;

import com.sqlproject.model.Reports;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReportsRowMapper implements RowMapper<Reports> {
    @Override
    public Reports mapRow(ResultSet rs, int rowNum) throws SQLException {
        Reports reports = new Reports();
        reports.setClientId(rs.getInt("client_id"));
        reports.setFirstName(rs.getString("first_name"));
        reports.setPatronymic(rs.getString("patronymic"));
        reports.setLastName(rs.getString("last_name"));
        reports.setCreditHistory(rs.getString("credit_history"));
        reports.setScore(rs.getInt("score"));

        return reports;
    }


}
