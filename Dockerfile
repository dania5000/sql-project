FROM openjdk:11-jdk
WORKDIR /app
COPY target/*.jar sql-project.jar
COPY src/main/resources/application.properties application.properties
CMD ["java", "-jar", "sql-project.jar"]
